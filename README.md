# ROS2 Humble Hawksbill
GPUバージョン

GPUをサポートしたROS2 Humble Hawksbillをdockerから簡単に使いたい。

## 事前の準備
お使いのコンピュータにDockerとDocker composeをインストールしてください。
下記の環境での動作を確認しています。

- Ubuntu 22.04.3 LTS
    - Docker version 24.0.7, build afdd53b
    - Docker Copmose version v2.21.0
- Ubuntu 20.04 LTS
    - Docker version 
    - Docker Copmose version

- MacBook Air M2 macOS Sonoma 14.0
    - Docker Desktop for Mac version 20.10.17, build 100c701
    - Docker Compose version v2.10.2

- Windows11 Education
    - 
    -

## 簡単に試してみる
Dockerイメージをクラウド上のDocker Hubからダウンロードすることで簡単に試すことができます。イメージのダウンロードには時間がかかるので気長に待ってください。

ダウンロードが終了したらdockerコマンドでdockerコンテナを起動できます。
```
$ cd ~
$ docker pull okdhryk/ros:humble-desktop-full-nvidia
$ docker run --rm -it --net host -e DISPLAY=$DISPLAY -e NO_AT_BRIDGE=1 okdhryk/ros:humble-desktop-full-nvidia
```

## インストール
```
$ cd ~
$ git clone git@gitlab.com:okadalaboratory/docker-images/humble.git -b nvidia humble-nvidia
$ cd humble-nvidia
$ docker compose build
```
イメージがokdhryk/ros:humble-desktop-full-nvidiaという名前で作成されます。
イメージ名を変更したい場合は、docker-compose.ymlを編集してください。


## 初めて実行する前の大事な注意
Dockerコンテナで行った、ディレクトリの作成やファイルの編集、パッケージのインストールなどはDockerイメージには反映されず、コンテナが破棄された時点で消えてしまいます。
オブジェクト指向で言うところのクラス（Dockerイメージ）とインスタンス（Dockerコンテナ）の関係です。

そこで、ホストコンピュータのディレクトリをコンテナにマウントし共有します。
このようにすれば、相互にマウントしたディレクトリ内での編集作業が消えることはありません。


### ホストコンピュータとコンテナ間で共有するディレクトリの作成
コンテナを初めて起動するする前に、事前の準備として下記の通り、ホストコンピュータにディレクトリを作成しておいてください。
```
$ cd ~
$ mkdir share
```
ここでは"share"という名前のディレクトリを作成しています。
共有するディレクトリの名前は .envファイルで下記のように指定しています。"share"以外の名前にしたい場合は.envファイルを編集してください。
```
WORKSPACE_DIR=/home/roboworks/share
```

### 事前に共有ディレクトリを作成しなかった場合
事前に共有ディレクトリを作成しなかった場合、Dockerシステムは共有ディレクトリを自動的に作成します。
ただし、自動的に作成された共有ディレクトリはroot権限で作成されるため、書き込みが自由にできないディレクトリになってしまいます。
そのような場合はコンテナを停止した後に、ホストコンピュータで下記のように共有したいディレクトリのユーザとグループを変更してください。
```
$ cd ~
$ ls -al share
合計 8
drwxr-xr-x  2 root   roor     4096 12月 31 23:29 .
drwxr-x--- 24 ユーザ名　グループ名 4096 12月 31 23:29 ..
```

```
$ sudo chown ユーザ名 share/
$ sudo chgrp グループ名 share/
```
共有ディレクトリのユーザ名とグループ名が変更されているか確認します。
```
$ ls -al share
合計 8
drwxr-xr-x  2 ユーザ名 グループ名 4096 12月 31 23:31 .
drwxr-x--- 24 ユーザ名 グループ名 4096 12月 31 23:31 ..
```

## コンテナの実行
下記のコマンドでコンテナを起動します
```
$ cd ~/humble-nvidia
$ docker compose up
```
ホストコンピュータの別の端末から起動中のコンテナに入るには下記のコマンドを実行します。
```
$ cd ~/humble-nvidia
$ docker compose exec humble-nvidia /bin/bash
```




![screenshot](https://gitlab.com/okadalaboratory/docker-images/ubuntu/focal/-/raw/images/%E3%82%BF%E3%82%99%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%88%E3%82%99__43_.png)




